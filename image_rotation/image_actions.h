//
// Created by Asus on 04.02.2021.
//
#include "bmp.h"

#ifndef IMAGE_ROTATION_IMAGE_ACTIONS_H
#define IMAGE_ROTATION_IMAGE_ACTIONS_H

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create (const uint64_t w, const uint64_t h);
void image_rotate(struct image* img);
void image_data_destroy(struct image* img);
void image_destroy(struct image* img);


#endif //IMAGE_ROTATION_IMAGE_ACTIONS_H

