#include "image_actions.h"
#include <malloc.h>

struct image image_create (const uint64_t w, const uint64_t h){

    return (struct image){
            .height=h,
            .width=w,
            .data = malloc(w * h * sizeof(struct pixel))
    };
}

void image_data_destroy(struct image* img) {
    free(img->data);
}
void image_destroy(struct image* img) {
    image_data_destroy(img);
    free(img);
}

void image_rotate(struct image* img) {
    struct pixel* new_data = malloc(img->height * img->width * sizeof(struct pixel));

    for (uint64_t i = 0; i < img->height; i++){
        for (uint64_t j = 0; j < img->width; j++){
            new_data[i + j * img->height] = img->data[j + (img->height - i - 1) * img->width];
        }
    }

    const uint64_t h = img->height;
    img->height = img->width;
    img->width = h;

    image_data_destroy(img);
    img->data = new_data;
}