#include "read_write_image.h"
#include "read_write_bmp.h"
#include <string.h>
#include <malloc.h>

#define FORMAT_HANDLERS_MAP_SIZE 1

struct format_handlers* find_handler(const char* formatType);
const char* get_suffix(const char* filename);

struct format_handlers format_handlers_map[] = {
    [BMP] = {.format_suffix = "bmp", .read_file = bmp_to_image, .write_file = image_to_bmp}
};

enum read_status read_image(const char* filename, struct image* image) {
    const char* format = get_suffix(filename);
    struct format_handlers* formatHandlers = find_handler(format);

    if(formatHandlers == NULL) return READ_FORMAT_HANDLER;

    return formatHandlers->read_file(filename, image);
}

enum write_status write_image(const char* filename, struct image* image) {
    const char* format = get_suffix(filename);
    struct format_handlers* formatHandlers = find_handler(format);

    if(formatHandlers == NULL) return WRITE_FORMAT_HANDLER;

    return formatHandlers->write_file(filename, image);
}


struct format_handlers* find_handler(const char* formatType) {

    for(size_t i = 0; i < FORMAT_HANDLERS_MAP_SIZE; i++) {
        if (strcmp(format_handlers_map[i].format_suffix, formatType) == 0) return &format_handlers_map[i];
    }

    return NULL;
}

const char* get_suffix(const char* filename) {
    int i = 0;
    char* sep = ".";

    for(;; i++) {
        if(filename[i] == *sep) {
            i++;
            break;
        }
    }

    return &filename[i];
}