#ifndef IMAGE_ROTATION_STATUS_H
#define IMAGE_ROTATION_STATUS_H

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_WRONG_FORMAT,
    READ_PATH_ERROR,
    READ_CLOSE_ERROR,
    READ_INVALID_HEADER,
    READ_FORMAT_HANDLER
};


enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_FILE_DOESNT_EXIST,
    WRITE_WRONG_FORMAT,
    WRITE_CLOSE_ERROR,
    WRITE_FORMAT_HANDLER
};

const char* read_status_map[];
const char* write_status_map[];

#endif //IMAGE_ROTATION_STATUS_H