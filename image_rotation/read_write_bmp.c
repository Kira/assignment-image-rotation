#include "read_write_bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>
#include "status.h"


static bool is_bmp_header_correct(const struct bmp_header* header){

    if (header->bfType != BFTYPEFX) return false;
    if (header->bfReserved != RESERVED ) return false;
    if (header->biCompression != BI_RGB) return false;
    if (header->biSize != BMPSIZEHEADER) return false;
    if (header->biBitCount != BITPERPIXEL) return false;
    if (header->biPlanes != BMPPLANES) return false;

    return true;
}


static uint64_t get_padding(const uint32_t width){
    const uint64_t buffer = width * sizeof(struct pixel) % 4;
    if ((width * sizeof(struct pixel) + buffer) % 4 != 0){
        return 4 - buffer;
    }
    return buffer;
}

static bool read_header( FILE* f, struct bmp_header* header ) {

    if(fread(header, sizeof(struct bmp_header), 1, f) != 1){
        return READ_INVALID_SIGNATURE;
    }
    if (!is_bmp_header_correct(header)){
        return READ_INVALID_HEADER;
    }
    if (fseek(f,header->bOffBits, 0)){
        return READ_INVALID_BITS;
    }

    return READ_OK;
}


enum read_status read_bmp_body(FILE* f, struct bmp_image* img) {
    img->data = malloc(sizeof (struct pixel) * img->header->biWidth * img->header->biHeight );

    for (uint64_t i = 0; i < img->header->biHeight; i++){
        fread(img->data + i * img->header->biWidth, sizeof(struct pixel), (size_t)img->header->biWidth, f);
        if (feof(f)) {
            free(img->data);
        }
        if (ferror(f)){
            free(img->data);
            return READ_INVALID_BITS;
        }
        fseek(f, get_padding(img->header->biWidth), 1);
    }

    return READ_OK;
}


static enum read_status read_bmp(FILE* f, struct bmp_image* bmpImg) {
    bmpImg->header = malloc(sizeof (struct bmp_header));

    enum read_status headerStatus = read_header(f, bmpImg->header);
    if(headerStatus != READ_OK) return headerStatus;

    enum read_status bodyStatus = read_bmp_body(f, bmpImg);
    if(bodyStatus != READ_OK) return bodyStatus;

    return READ_OK;
}

static struct bmp_header* create_header(const uint32_t width, const uint32_t height ) {
    struct bmp_header* header = malloc(sizeof (struct bmp_header));

    header->bfType = BFTYPEFX;
    header->bfReserved = RESERVED;
    header->bOffBits =  sizeof (struct bmp_header);
    header->bfileSize = header->bOffBits + height * (width * sizeof( struct pixel ) + get_padding(width) );
    header->biSize = BMPSIZEHEADER;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BMPPLANES;
    header->biBitCount = BITPERPIXEL;
    header->biCompression = BI_RGB;
    header->biSizeImage = 0;
    header->biClrUsed = BICLRUSED;
    header->biClrImportant = BICLRIMPORTANT;

    return header;
}

enum write_status* image_to_bmp(const char* outFileName, struct image* img) {
    FILE *f = fopen(outFileName, "wb");

    if (f == NULL) return WRITE_FILE_DOESNT_EXIST;

    struct bmp_image bmp_image = {0};

    bmp_image.header = create_header(img->width, img->height);
    bmp_image.data = malloc(sizeof(struct pixel) * img->width * img->height );

    for (size_t i = 0; i < img->height * img->width; i++) {
        bmp_image.data[i] = img->data[i];
    }

    if (fwrite(bmp_image.header, sizeof(struct bmp_header), 1, f)<1) return WRITE_WRONG_FORMAT;

    for (uint64_t i = 0; i < bmp_image.header->biHeight; i++) {
        if (fwrite( bmp_image.data + i * bmp_image.header->biWidth, sizeof(struct pixel), bmp_image.header->biWidth, f ) < bmp_image.header->biWidth ) return WRITE_ERROR;
        if (fseek(f,get_padding(bmp_image.header->biWidth),1)) return WRITE_ERROR;
    }

    free(bmp_image.data);
    free(bmp_image.header);

    image_data_destroy(img);

    if(fclose(f) != 0) return WRITE_ERROR;
}

enum read_status bmp_to_image(const char* inFileName, struct image* img) {
    FILE *f = fopen(inFileName, "rb");

    if (f == NULL) return READ_PATH_ERROR;

    struct bmp_image bmpImage = {0};

    enum read_status readBmpStatus = read_bmp(f, &bmpImage);
    if (readBmpStatus != READ_OK) return readBmpStatus;

    *img = image_create(bmpImage.header->biWidth, bmpImage.header->biHeight);

    img->width = bmpImage.header->biWidth;
    img->height = bmpImage.header->biHeight;
    for (size_t i = 0; i < img->height * img->width; i++) {
        img->data[i] = bmpImage.data[i];
    }

    free(bmpImage.header);
    free(bmpImage.data);

    if(fclose(f) != 0) return READ_CLOSE_ERROR;
}



