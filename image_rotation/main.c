#include "image_actions.h"
#include "read_write_image.h"
#include <malloc.h>
#include "utils.h"

#define SIGNATURE "You should use this command with next signature image_rotate [input_file] [output_file]\n "\
                  "if you want to apply rotation to the input file yo can use next signature image_rotate [input_file] \n"


int main(int argc, char** argv ) {
    char* inFileName;
    char* outFileName;

    if (argc<2 || argc > 3){
        err(SIGNATURE);
    }
    if (argc ==2 ){
        inFileName = outFileName =  argv[1];
    }
    if (argc ==3 ){
        inFileName = argv[1];
        outFileName = argv[2];
    }


    struct image* image = malloc(sizeof (struct image));

    enum read_status readStatus = read_image(inFileName, image);
    if( readStatus != READ_OK) err(read_status_map[readStatus]);

    image_rotate(image);

    enum write_status writeStatus = write_image(outFileName, image);
    if( writeStatus != WRITE_OK) err(write_status_map[writeStatus]);

    free(image);

    return 0;
}
