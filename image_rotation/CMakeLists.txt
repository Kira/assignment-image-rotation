cmake_minimum_required(VERSION 3.17)
project(image_rotation C)

set(CMAKE_C_STANDARD 99)

add_executable(image_rotation main.c bmp.c bmp.h read_write_image.c read_write_image.h image_actions.c image_actions.h read_write_bmp.c read_write_bmp.h utils.c utils.h status.c status.h)