//
// Created by Asus on 04.02.2021.
//
#include "image_actions.h"
#include "status.h"
#include "read_write_bmp.h"

#ifndef IMAGE_ROTATION_READ_WRITE_H
#define IMAGE_ROTATION_READ_WRITE_H
enum formats {
    BMP = 0
};

struct format_handlers {
    const char* format_suffix;
    enum read_status (*read_file)(const char*, struct image*);
    enum write_status (*write_file)(const char*, struct image*);
};

enum read_status read_image(const char* filename, struct image* image);
enum write_status write_image(const char* filename, struct image* image);

#endif //IMAGE_ROTATION_READ_WRITE_H
