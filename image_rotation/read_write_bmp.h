#include "bmp.h"
#include "image_actions.h"


#ifndef IMAGE_ROTATION_READ_WRITE_BMP_H
#define IMAGE_ROTATION_READ_WRITE_BMP_H

#define BFTYPEFX 0x4D42
#define BI_RGB 0
#define BMPSIZEHEADER 40
#define BMPPLANES 1
#define BITPERPIXEL 24
#define RESERVED 0
#define BICLRUSED 0
#define BICLRIMPORTANT 0

enum read_status bmp_to_image(const char* inFileName, struct image* img);
enum write_status* image_to_bmp(const char* outFileName, struct image* img);

#endif //IMAGE_ROTATION_READ_WRITE_BMP_H