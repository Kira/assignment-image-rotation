#include "status.h"

const char* read_status_map[] = {
    [READ_OK]="Your input file was successfully read",
    [READ_CLOSE_ERROR] = "Error closing file",
    [READ_PATH_ERROR] = "Read-path not exist",
    [READ_INVALID_BITS] = "This image was corrupted",
    [READ_INVALID_SIGNATURE] = "Input file signature is invalid",
    [READ_WRONG_FORMAT] = "Wrong input file format",
    [READ_INVALID_HEADER] = "Wrong header format",
    [READ_FORMAT_HANDLER] = "No handlers has been found"
};

const char* write_status_map[] = {
    [WRITE_OK]="Output file was updated",
    [WRITE_CLOSE_ERROR] = "Error closing file",
    [WRITE_ERROR] = "The process of writing wasn't finished due to corrupted output file",
    [WRITE_WRONG_FORMAT] = "Output file format error",
    [WRITE_FILE_DOESNT_EXIST] = "output file doesn't exist, check your path",
    [WRITE_FORMAT_HANDLER] = "No handlers has been found"
};